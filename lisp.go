package main

import (
	"fmt"
	"reflect"
	B "bytes"
)


// universal function
func eval(e, a interface{}) interface{} {
	switch {
	case atom(e): 
		if e == "NIL" { return nil }
		if e == "T" { return "T" }
		return cdr(assoc(e, a))
	case atom(car(e)):
		switch str(car(e)) {
		case "quote": return cadr(e)
		case "car"  : return car(eval(cadr(e), a))
		case "cdr"  : return cdr(eval(cadr(e), a))
		case "cadr" : return cadr(eval(cadr(e), a))
		case "caddr": return caddr(eval(cadr(e), a))
		case "caar" : return caar(eval(cadr(e), a))
		case "cadar": return cadar(eval(cadr(e), a))
		case "caddar": return caddar(eval(cadr(e), a))
		case "atom" : return atom(eval(cadr(e), a))
		case "null" : return null(eval(cadr(e), a))
		case "cons" : return cons(eval(cadr(e), a), eval(caddr(e), a))
		case "eq"   : return eval(cadr(e), a) == eval(caddr(e), a)
		case "cond" : return evcond(cdr(e), a)
		default     : return eval(cons(cdr(assoc(car(e), a)), cdr(e)), a)
		}
	case caar(e) == "lambda" :
		return eval(caddar(e), ffappend(pairup(cadar(e),evlis(cdr(e), a)), a))
	case caar(e) == "label"  :
		return eval(cons(caddar(e), cdr(e)), cons(cons(cadar(e), car(e)), a))
	}
	panic("Error -- can't eval: " + str(e))
}

// helper for eval()
func assoc(e, a interface{}) interface{} {
	if null(a) { return nil }
	if e == caar(a) { return car(a) }
	return assoc(e, cdr(a))
}

func null(e interface{}) bool {
	return e == nil || reflect.ValueOf(e).IsNil()
}

func ffappend(u, v interface{}) interface{} {
	if null(u) { return v }
	return cons(car(u), ffappend(cdr(u), v))
}

func pairup(u, v interface{}) interface{} {
	if null(u) { return nil }
	return cons(cons(car(u), car(v)), pairup(cdr(u), cdr(v)))
}

func evlis(u, a interface{}) interface{} {
	if null(u) { return nil }
	return cons(eval(car(u), a), evlis(cdr(u), a))
}

func evcond(u, a interface{}) interface{} {
	if eval(caar(u), a) != nil { return eval(cadar(u), a) }
	return evcond(cdr(u), a)
}

func main() {
	fmt.Println(equal(car(cons(1,2)), 1))
	fmt.Println(equal(cdr(cons(1,2)), 2))
	fmt.Println(atom(1))
	fmt.Println(!atom(cons(1,2)))
	fmt.Println(!atom(cons(1,2)))
	fmt.Println(!atom(nil))

	fmt.Println(equal(nil, eval("NIL", nil)))
	fmt.Println(equal("T", eval("T", nil)))
	fmt.Println(equal(1, eval(cons("quote", cons(1, nil)), nil)))
	fmt.Println(equal(1, eval(list("quote", 1), nil)))
	fmt.Println(equal(1, eval(list("car", list("quote", list(1, 2))), nil)))

	fmt.Println(equal("1", eval(parse("(cond ((atom (quote x)) (quote 1)) ((quote T) (quote 2)))"), nil)))
}

// list operations : car/cdr/cons
func cons(v1, v2 interface{}) []interface{} {
	return []interface{}{v1,v2}
}

func car(v interface{}) interface{} {
	return v.([]interface{})[0]
}

func cdr(v interface{}) interface{} {
	return v.([]interface{})[1]
}

// check : equal/atom
func equal(v1, v2 interface{}) bool {
	return str(v1) == str(v2)
}

// should not be nil or  a slice type
func atom(v interface{}) bool {
	if v == nil { return false }
	_, ok := v.([]interface{})
	return !ok 
}

// helper
func str(v interface{}) string {
	if atom(v) { return fmt.Sprintf("%v", v) }

	var buf B.Buffer
	buf.WriteString("(")
	for !null(v) {
		buf.WriteString(str(car(v)))
		v = cdr(v)	
		if null(v) {break}
		buf.WriteString(" ")
	}
	buf.WriteString(")")
	return buf.String()
}

func list(v ...interface{}) interface{} {
	return listV(v)
}

func listV(v []interface{}) interface{} {
	if len(v) == 0 { return nil }
	return cons(v[0], listV(v[1:]))
}
	
// c[ad]+r
func cadr(v interface{}) interface{} { return car(cdr(v)) }
func caddr(v interface{}) interface{} { return car(cdr(cdr(v))) }
func caar(v interface{}) interface{} { return car(car(v)) }
func cadar(v interface{}) interface{} { return car(cdr(car(v))) }
func caddar(v interface{}) interface{} { return car(cdr(cdr((car(v))))) }
